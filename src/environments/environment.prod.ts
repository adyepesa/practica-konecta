export const environment = {
  production: true,
  baseUrlAPI1: 'https://rickandmortyapi.com/api/character',
  baseUrlAPI2: 'https://rickandmortyapi.com/api/episode'
};
