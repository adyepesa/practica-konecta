import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FormSearchComponent } from './shared/components/form-search/form-search.component';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CharacterService } from './shared/services/character.service';
import { EpisodesService } from './shared/services/episodes.service';
import { FormsModule } from '@angular/forms';
import { CharactersListModule } from './components/pages/characters/characters-list/characters-list.module';

const routes:Routes=[{ path: 'home', loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule) }, { path: 'characters-list', loadChildren: () => import('./components/pages/characters/characters-list/characters-list.module').then(m => m.CharactersListModule) }, { path: 'episodes-list', loadChildren: () => import('./components/pages/episodes/episodes-list/episodes-list.module').then(m => m.EpisodesListModule) }, { path: 'episodes-details', loadChildren: () => import('./components/pages/episodes/episodes-details/episodes-details.module').then(m => m.EpisodesDetailsModule) }, { path: 'characters-details', loadChildren: () => import('./components/pages/characters/characters-details/characters-details.module').then(m => m.CharactersDetailsModule) }];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FormSearchComponent,
    CharactersListModule
    
    
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule

  ],
  providers: [
    CharacterService,
    EpisodesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

