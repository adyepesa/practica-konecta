import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { CharactersModule } from '../characters/characters.module';
import { EpisodesModule } from '../episodes/episodes.module';
import { FormSearchComponent } from 'src/app/shared/components/form-search/form-search.component';


const routes: Routes = [
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [HomeComponent,],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CharactersModule,
    EpisodesModule,
    
  ]
})
export class HomeModule { }
