import { Component, OnInit } from '@angular/core';
import { Characters } from 'src/app/shared/interface/characters.interface';
import { CharacterService } from 'src/app/shared/services/character.service';
import { take } from "rxjs/operators"
type RequestInfo ={
  next: string;
}
@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss']
})
export class CharactersListComponent implements OnInit {
  characters: Characters[]=[];
  info: RequestInfo = {
    next: null
  }
  private pageNum=1;
  private query:string;


  constructor(private characterSvc: CharacterService) { }

  ngOnInit(): void {
  }

  private getDataFromService ():void{
    this.characterSvc.searchCharacters(this.query, this.pageNum)
    .pipe(
      take(1)
    ).subscribe( (res:any) =>  {
      const { info, results } = res;
      this.characters = [...this.characters, ...results ];
      this.info=info;
    }

    )
  }
}
