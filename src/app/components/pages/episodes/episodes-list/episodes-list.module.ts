import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EpisodesListComponent } from './episodes-list.component';


const routes: Routes = [
  { path: '', component: EpisodesListComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EpisodesListModule { }
