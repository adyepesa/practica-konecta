import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EpisodesDetailsComponent } from './episodes-details.component';


const routes: Routes = [
  { path: '', component: EpisodesDetailsComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EpisodesDetailsModule { }
