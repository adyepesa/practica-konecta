import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EpisodesDetailsComponent } from './episodes-details/episodes-details.component';
import { EpisodesListComponent } from './episodes-list/episodes-list.component';
import { RouterModule } from "@angular/router";



@NgModule({
  declarations: [
    EpisodesDetailsComponent,
    EpisodesListComponent
  ],
  imports: [
    CommonModule, RouterModule
  ],
  exports:[
    EpisodesDetailsComponent,
    EpisodesListComponent
  ]
})
export class EpisodesModule { }
