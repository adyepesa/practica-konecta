import { ArrayType } from "@angular/compiler";

export interface Characters {
    id: number;
    nombre: string;
    image: string;
    especie: string;
    episodios: ArrayType;
    gender:string;
    creado: string;
    estatus: string;



}