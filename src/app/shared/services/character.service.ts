import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment'
import { Characters } from '../interface/characters.interface';
@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  searchCharacters(query='', page=1){
    return this.http.get<Characters[]>(`${environment.baseUrlAPI1}/name=${query}&page=${page}`);
  };
  

  getDetails(id:number) {
    return this.http.get<Characters>(`${environment.baseUrlAPI1}/${id}`);
  }
}
