import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment'
import { Episodes } from '../interface/episodes.interface';

@Injectable({
  providedIn: 'root'
})
export class EpisodesService {

  constructor(private http: HttpClient) { }
  searchEpisodes(query='', page=1){
    return this.http.get<Episodes[]>(`${environment.baseUrlAPI2}/name=${query}&page=${page}`);
  }
  
}
